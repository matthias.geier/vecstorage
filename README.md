# VecStorage

Re-use the memory for vectors containing values with different lifetimes

In some situations, e.g. when writing real-time audio software, memory needs to be pre-allocated.
The `Vec` data structure from Rust's standard library can be used for this purpose.
In some situations, however, you want to re-use the same `Vec` for data that has different lifetimes
and Rust's type system doesn't allow this.
This crate intends to help you overcome this problem.


Contributing
------------

We welcome contributions, both in the form of issues and in the form of merge requests.
Before opening a merge request, please open an issue first so that you know whether a subsequent
merge request would likely be approved.


License
-------

Vecstorage is distributed under the terms of the MIT license or the Apache License (Version 2.0), 
at your choice.
For the application of the MIT license, the examples included in the doc comments are not
considered "substatial portions of this Software".
